# Go CD Server and Agent #

This repository contains a Vagrantfile that will setup an two ubuntu bionic boxes and install GoCD Server one one, and GoCD Agent and Docker on the second one.

### How do I get set up? ###

* Install VirtualBox
* Install Vagrant
* Clone this repository and cd into it

```bash
vagrant up
vagrant ssh server
vagrant ssh agent
```

* connect to http://192.168.33.10:8153
